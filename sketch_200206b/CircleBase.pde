class CircleBase {
  public int xPosition = 0;
  public int yPosition = 0;
  public int diameter = 0;
  public int[] circleColor = {0,0,0};
  public boolean isMovingClockwise = false;
  public boolean isPlanet = false;
  public int distanceToSun = 0;
  public float speed = 0;
  public String Name = "Test";
  public boolean nameVisibile;
  
  private int iteration = 0;
  
  CircleBase(
    int xPosition, 
    int yPosition, 
    int diameter,
    int[] circleColor,
    boolean isMovingClockwise,
    boolean isPlanet,
    int distanceToSun,
    float speed,
    String Name)
    {
    this.xPosition = xPosition;
    this.yPosition = yPosition;
    this.diameter = diameter;
    this.circleColor = circleColor;
    this.isMovingClockwise = isMovingClockwise;
    this.isPlanet = isPlanet;    
    this.distanceToSun = distanceToSun;
    this.speed = speed;
    this.Name = Name;
}

  public void keyPressed()
  {
    //Namen der PLANETEN wird angezeigt durch Drücken von ENTER
    if (keyCode == ENTER) 
    {
      if(!nameVisibile)
      {
        nameVisibile = true;
      }
      else
      {
        nameVisibile = false;
      }
    }  
  }
  
  // Draw the planet
  void draw() {
    fill(
      this.circleColor[0], 
      this.circleColor[1], 
      this.circleColor[2]
    );
    circle(this.xPosition, this.yPosition, this.diameter);
      if(nameVisibile)
    {
      textSize(20);
      text(Name, xPosition - (diameter / 2), yPosition - (diameter / 2));
    }
  }
  
  void move() {
    if (this.isPlanet) {
      this.iteration++;
      
      //Umlaufwinkel berechnen
      int directionIndicator = 1;
      if (this.isMovingClockwise) {
        directionIndicator = -1;
      }
      ///////////////////////////////////////////////////////////// Quelle: Beispielprojekt von Herrn Birkenhagen (liegt im Tauschordner)
      float alpha = ((float) this.iteration) / 10 * (umlaufInErdjahren/(ErdjahrInSekunden)) * this.speed * (directionIndicator);
      
      //Aus Umlaufwinkel die Koordinaten berechnen
      this.xPosition = Math.round(xZentrum +sin(alpha)*distanceToSun);
      this.yPosition = Math.round(yZentrum+cos(alpha)*distanceToSun);
      /////////////////////////////////////////////////////////////
      
      this.draw();
    }
  }
}
