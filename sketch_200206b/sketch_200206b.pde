PImage bg;

CircleBase pluto;
CircleBase sun;
CircleBase earth;
CircleBase mars;
CircleBase mercury;
CircleBase venus;
CircleBase jupiter;
CircleBase saturn;
CircleBase uranus;
CircleBase neptune;
CircleBase Name;

int windowWidth = 900;
int windowHeight = 900;

float umlaufInErdjahren = 1;
float ErdjahrInSekunden = 10;
int xZentrum = windowWidth/2;
int yZentrum = windowHeight/2;

void setup() {
  size(900, 900); 
  bg = loadImage("images/background.png");

  // https://astrokramkiste.de/planeten-tabelle

  // Optimized speed of planet = ((originalSpeed / 1000) / 100) × 5
  

  // Create Sun

  int sunDiameter = 80;
  int[] sunColor = {255, 165, 0};
  int sunXPosition = (windowWidth / 2);
  int sunYPosition = (windowHeight / 2);
  int sunSpeed = 0;
  sun = new CircleBase(sunXPosition, sunYPosition, sunDiameter, sunColor, true, false, 0, sunSpeed, "Sun");
  
  // Create Mercury

  int mercuryDiameter = 25;
  int[] mercuryColor = {213, 180, 99};
  int mercuryXPosition = calculatePlanetXPosition(mercuryDiameter);
  int mercuryYPosition = calculatePlanetYPosition(mercuryDiameter);
  int mercuryDistanceToSun = calculateDistanceToSun(mercuryDiameter, -5);
  float mercurySpeed = 8.6166;
  mercury = new CircleBase(mercuryXPosition, mercuryYPosition, mercuryDiameter, mercuryColor, false, true, mercuryDistanceToSun, mercurySpeed, "Mercury");
  
  //Create Venus 

  int venusDiameter = 40;
  int[] venusColor = {224, 169, 42};
  int venusXPosition = calculatePlanetXPosition(venusDiameter);
  int venusYPosition = calculatePlanetYPosition(venusDiameter);
  int venusDistanceToSun = calculateDistanceToSun(venusDiameter, 20);
  float venusSpeed = 6.3036;
  venus = new CircleBase(venusXPosition, venusYPosition, venusDiameter, venusColor, false, true, venusDistanceToSun, venusSpeed, "Venus");
  
  // Create earth
  
  int earthDiameter = 50;
  int[] earthColor = {49, 95, 255};
  int earthXPosition = calculatePlanetXPosition(earthDiameter);
  int earthYPosition = calculatePlanetYPosition(earthDiameter);
  int earthDistanceToSun = calculateDistanceToSun(earthDiameter, 60);
  float earthSpeed = 5.3604;
  earth = new CircleBase(earthXPosition, earthYPosition, earthDiameter, earthColor, false, true, earthDistanceToSun, earthSpeed, "Earth");

  // Create Mars
  
  int marsDiameter = 26;
  int[] marsColor = {233, 126, 11};
  int marsXPosition = calculatePlanetXPosition(marsDiameter);
  int marsYPosition = calculatePlanetYPosition(marsDiameter);
  int marsDistanceToSun = calculateDistanceToSun(marsDiameter, 130);
  float marsSpeed = 4.3434;
  mars = new CircleBase(marsXPosition, marsYPosition, marsDiameter, marsColor, false, true, marsDistanceToSun, marsSpeed, "Mars");

  // Create Jupiter

  int jupiterDiameter = 65;
  int[] jupiterColor = {158, 192, 215};
  int jupiterXPosition = calculatePlanetXPosition(jupiterDiameter);
  int jupiterYPosition = calculatePlanetYPosition(jupiterDiameter);
  int jupiterDistanceToSun = calculateDistanceToSun(jupiterDiameter, 145);
  float jupiterSpeed = 2.3526;
  jupiter = new CircleBase(jupiterXPosition, jupiterYPosition, jupiterDiameter, jupiterColor, false, true, jupiterDistanceToSun, jupiterSpeed, "Jupiter");
    
  // Create Saturn

  int saturnDiameter = 45;
  int[] saturnColor = {172, 173, 103};
  int saturnXPosition = calculatePlanetXPosition(saturnDiameter);
  int saturnYPosition = calculatePlanetYPosition(saturnDiameter);
  int saturnDistanceToSun = calculateDistanceToSun(saturnDiameter, 225);
  float saturnSpeed = 1.7442;
  saturn = new CircleBase(saturnXPosition, saturnYPosition, saturnDiameter, saturnColor, false, true, saturnDistanceToSun, saturnSpeed, "Saturn");

  // Create Uranus

  int uranusDiameter = 42;
  int[] uranusColor = {199, 237, 238};
  int uranusXPosition = calculatePlanetXPosition(uranusDiameter);
  int uranusYPosition = calculatePlanetYPosition(uranusDiameter);
  int uranusDistanceToSun = calculateDistanceToSun(uranusDiameter, 275);
  float uranusSpeed = 1.2258;
  uranus = new CircleBase(uranusXPosition, uranusYPosition, uranusDiameter, uranusColor, false, true, uranusDistanceToSun, uranusSpeed, "Uranus");

  // Create Neptune

  int neptuneDiameter = 34;
  int[] neptuneColor = {71, 113, 251};
  int neptuneXPosition = calculatePlanetXPosition(neptuneDiameter);
  int neptuneYPosition = calculatePlanetYPosition(neptuneDiameter);
  int neptuneDistanceToSun = calculateDistanceToSun(neptuneDiameter, 325);
  float neptuneSpeed = 0.9774;
  neptune = new CircleBase(neptuneXPosition, neptuneYPosition, neptuneDiameter, neptuneColor, false, true, neptuneDistanceToSun, neptuneSpeed, "Neptune");

  // Create Pluto

  int plutoDiameter = 25;
  int[] plutoColor = {100, 102, 140};
  int plutoXPosition = calculatePlanetXPosition(plutoDiameter);
  int plutoYPosition = calculatePlanetYPosition(plutoDiameter);
  int plutoDistanceToSun = calculateDistanceToSun(plutoDiameter, 365);
  float plutoSpeed = 0.8496;
  pluto = new CircleBase(plutoXPosition, plutoYPosition, plutoDiameter, plutoColor, false, true, plutoDistanceToSun, plutoSpeed, "Pluto");
  
  // Initially draw all planets

  sun.draw();
  mercury.draw();
  venus.draw();
  earth.draw();
  mars.draw();
  jupiter.draw();
  saturn.draw();
  uranus.draw();
  neptune.draw();
  pluto.draw();
}

void draw() {
  background(bg); 
  sun.draw();
  
  mercury.move();  
  venus.move();
  earth.move();
  mars.move();
  jupiter.move();
  saturn.move();
  uranus.move();
  neptune.move();
  pluto.move();

  // Display info text
  this.drawHintText();
}

public void keyPressed(){
  sun.keyPressed();
  pluto.keyPressed();
  earth.keyPressed();
  mars.keyPressed();
  mercury.keyPressed();
  venus.keyPressed();
  jupiter.keyPressed();
  saturn.keyPressed();
  uranus.keyPressed();
  neptune.keyPressed();
  
}

int calculatePlanetXPosition(int planetDiameter) {
  return (windowWidth / 2) + (planetDiameter / 2);
}

int calculatePlanetYPosition(int planetDiameter) {
  return (windowHeight / 2) + (planetDiameter / 2);
}

int calculateDistanceToSun(int planetDiameter, int individualDistance) {
  return (sun.diameter / 2) + planetDiameter + individualDistance;
}

void drawHintText() {
  textSize(20);
  fill(255, 255, 255);
  text("Press Enter to show names", 30, 30);
}
