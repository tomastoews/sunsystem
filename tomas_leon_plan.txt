Programstruktur:

Klassen:

CircleBase
  - xPosition: int
  - yPosition: int
  - width: int
  - height: int
  - color: String
  - rotationSpeed: int
  - movingSpeed: int
  - isMovingClockwise: Boolean
  - draw()

Sun: extends CircleBase

Planet: extends CircleBase
  - movingSpeed: int
  - distanceToSun: int

Star: extends CircleBase

Moon: extends CircleBase
  - belongsToPlanet: Planet
  - distanceToPlanet: int
